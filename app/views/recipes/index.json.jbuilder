json.array!(@recipes) do |recipe|
  json.extract! recipe, :id, :title, :ingredients, :directions, :time, :difficulty, :output, :genre, :cost, :chef_id
  json.url recipe_url(recipe, format: :json)
end
