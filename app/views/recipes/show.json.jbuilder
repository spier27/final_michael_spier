json.extract! @recipe, :id, :title, :ingredients, :directions, :time, :difficulty, :output, :genre, :cost, :chef_id, :created_at, :updated_at
