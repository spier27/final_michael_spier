class Recipe < ActiveRecord::Base
  belongs_to :chef
  validates :title, presence:true, length:{maximum: 50}
  validates :ingredients, presence:true
  validates :directions, presence:true
  validates :time, presence:true
  validates :difficulty, presence:true
  validates :output, presence:true
  validates :genre, presence:true
  validates :cost, presence:true
  
  
end
