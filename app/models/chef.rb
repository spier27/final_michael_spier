class Chef < ActiveRecord::Base
  validates :name, presence:true, length: {maximum: 30}
  validates :stars, presence:true
  has_many :recipes

end
