class VisitorsController < ApplicationController
  def index
    @recipes = Recipe.all
  end

  def easy
    @recipes = Recipe.all
  end

  def difficult
    @recipes = Recipe.all
  end

  def cheap
    @recipes = Recipe.all
  end
  
  def chef1
    @recipes = Recipe.all
    @chef = Chef.find_by(id: 1)
  end
  def chef2
    @recipes = Recipe.all
    @chef = Chef.find_by(id: 2)
  end
  def chef3
    @recipes = Recipe.all
    @chef = Chef.find_by(id: 3)
  end
end
